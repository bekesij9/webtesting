# Webtesting

Stuff for presenting functional testing

## First attempt: Using Nightwatch (i.e. Javascript)

## Preliminary Info

Selenium Server is a Java application which Nightwatch uses to connect
to the various browsers.

So we do not use SS, because we do not want to conjure browser instances and windows all over the place...

Instead we use `Phantom.js`.


## Second attempt: Using Protractor and Jasmine (as test framwework)

Because Nightwatch did not work well with Angular/Phaidra
